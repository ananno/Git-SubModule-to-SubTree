#!/bin/bash

# Author: Syed Hasibur Rahman
# Usage: Automatically find nested submodules from a git repository and convert it to a git subtree
# Created: 2018-06-21
# Modifed: 2018-06-22

gitmodules="/tmp/.gitmodules_new"
base=`pwd`
files=`find . -name ".gitmodules" -not -path "./.gitmodules" | sort -n`


echo "#######################################################"
echo "# Adding nested submodules: $f"
echo "#######################################################"
cat .gitmodules > $gitmodules
for f in $files;
do
    cat $f |
        sed -e 's@path = @path = '`echo $f | cut -c 3- | awk -F "/.gitmodules" -v OFS="" '{print$1}'`'/@g' |
        sed -e 's@submodule "@submodule "'`echo $f | cut -c 3- | awk -F "/.gitmodules" -v OFS="" '{print$1}'`'/@g' >> $gitmodules;
done
echo "#######################################################"
echo "# Removing root submodules ..."
echo "#######################################################"
cat .gitmodules | while read i
do
    if [[ $i == \[submodule* ]]; then
        # extract the module's prefix
        mpath="$(echo $i | cut -d\" -f2)"
        echo "Removing Submodule: $mpath"

        # skip two lines
        read i; read i;

        # extract the url of the submodule
        murl=$(echo $i|cut -d\= -f2|xargs)

        # extract the module name
        mname=$(basename $mpath)

        # deinit the module
        git submodule deinit $mpath

        # remove the module from git
        git rm -r --cached $mpath

        # remove the module from the filesystem
        rm -rf $mpath

        # commit the change
        git commit -a -m "Removed $mpath submodule"
    fi
done
git rm .gitmodules
# commit the change
git commit -a -m "Removed .gitmodule file"

echo "#######################################################"
echo "# Adding subtrees ..."
echo "#######################################################"
cat $gitmodules | while read i
do
    if [[ $i == \[submodule* ]] ; then
        echo "------------------------------------------------------"
        # extract the module's prefix
        mpath="$(echo $i | cut -d\" -f2)"

        # skip two lines
        read i; read i;
        
        # extract the url of the submodule
        murl=$(echo $i|cut -d\= -f2|xargs)

        # extract the module name
        mname=$(basename $mpath)

        echo "# Submodule:"
        echo "# Path: $mpath"
        echo "# URL: $murl"
        echo "# Name: $mname"
        
        # add the remote
        echo "------------------------------------------------------"
        echo "Adding remote repo [$mname]: $murl"
        git remote add $mname $murl

        # add the subtree
        echo "------------------------------------------------------"
        echo "# Adding SubTree:"
        echo "# Prefix: $mpath"
        echo "# Name: $mname"
        rm -rf $mpath
        # commit the change
        git commit -a -m "removing $mpath"
        git subtree add --prefix $mpath $mname master --squash
        # fetch the files
        git fetch $murl master

        # commit the change
        git commit -a --amend -m "Adding SubTree $mpath"
        echo "------------------------------------------------------"
    fi
done
rm -rf $gitmodules

echo
echo "All Complete...!!!"

